import { clsx, type ClassValue } from 'clsx'
import { twMerge } from 'tailwind-merge'

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs))
}

export function formatEpochToTime(epoch: number) {
  const date = new Date(epoch * 1000) // Convert epoch to milliseconds
  const hours = date.getHours()
  const minutes = date.getMinutes()
  const ampm = hours >= 12 ? 'PM' : 'AM'
  const formattedHours = hours % 12 || 12 // Convert 24h to 12h format and handle midnight
  const formattedMinutes = minutes < 10 ? '0' + minutes : minutes // Add leading zero if needed

  return `${formattedHours}:${formattedMinutes} ${ampm}`
}