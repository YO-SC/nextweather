'use server'

import { redirect } from 'next/navigation'

export async function searchByZipCode(formData: FormData) {
  const zipCode = formData.get('zipCode')

  redirect(`/?zip=${zipCode}`)
}
