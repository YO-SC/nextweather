import { HourWeatherCard, TodayWeatherCard } from '@/components/cards'
import { ZipForm } from '@/components/forms'
import { TypoH1, TypoH2, TypoP } from '@/components/typography'
import { WeatherData } from '@/types/weather-api'

async function getWeather(method: 'current' | 'forecast', zipCode: string) {
  const response = await fetch(
    `https://api.weatherapi.com/v1/${method}.json?key=${process?.env?.WEATHERAPI_KEY}&q=${zipCode}`,
    {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
      cache: 'no-store', //we want new data always, not cached data
    }
  )
    .then((res) => res.json())
    .catch((error) => {
      console.error(`Error ${error}`)
    })

  return response
}

export default async function Home({
  searchParams,
}: {
  searchParams: { zip: string }
}) {
  const { zip } = searchParams
  const data: WeatherData = await getWeather('forecast', zip)

  return (
    <main className="flex flex-col gap-4">
      <TypoP>Enter your zip code to get weather information.</TypoP>

      <ZipForm />

      {data?.error?.code === 1006 && <div>{data?.error?.message}</div>}

      {data?.current && (
        <>
          <TypoH2 className="font-extrabold">Current</TypoH2>

          <TodayWeatherCard
            currentData={data.current}
            locationData={data.location}
          />
        </>
      )}

      {data?.forecast && (
        <>
          <TypoH2 className="font-extrabold">Hourly</TypoH2>

          {/* forecastday just contains 1 element always, idk why since i don't code the WeatherAPI */}
          {/* math.floor to the current date in order to properly compare it. WeatherAPI's epoch does not include milliseconds. This would include extra value to Date.now() making it higher than h.time_epoch */}
          <div className="grid grid-cols-1 md:grid-cols-4 gap-2 md:gap-4">
            {data.forecast?.forecastday[0]?.hour
              .filter(
                // will render from current hour onwards
                (hour) => hour.time_epoch >= Math.floor(Date.now() / 1000)
              )
              .map((hour, index) => (
                <HourWeatherCard key={index} hourData={hour} />
              ))}
          </div>
        </>
      )}
    </main>
  )
}
