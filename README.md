# NextWeather - WeatherAPI Application

This project is a NextJS application that leverages the [Weather API](https://www.weatherapi.com/) to enable users to see their current weather by providing a Zip Code. It features Server-Side Rendering (SSR) to improve performance. The frontend UI is custom made using [Tailwind CSS](https://tailwindcss.com/).

## Features

- **See current weather conditions**: By providing a Zip Code, users can see the current weather in that area.
- **Fully SSR**: Enhances loading times.
- **Responsive UI**: Built with [Tailwind CSS](https://tailwindcss.com/) for a modern and responsive user experience.

## Getting Started

To get this project up and running on your local machine, follow these steps:

### Prerequisites

- Node.js (version 20 or above)
- [bun.sh](https://bun.sh/)

### Installation

1. Clone the repository:

```bash
git clone https://gitlab.com/YO-SC/nextweather.git
```

2. Navigate to the project directory:

```bash
cd nextmovies
```

3. Install dependencies:

```bash
bun install
```

4. Create a `.env.local` file at the root of the project and add your OMDb API key (an `.env.local.example` is in the root of the project):

```env
WEATHERAPI_KEY=your_api_key_here
```

5. Start the development server:

```bash
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## License

This project is open source and available under the MIT License.

## Created by

[YO-SC](https://gitlab.com/YO-SC)
