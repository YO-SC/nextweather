import { cn } from '@/lib/utils'
import { TypoHProps } from './types'

export function TypoH1(props: TypoHProps) {
  const { className, children, ...rest } = props

  return (
    <h1 className={cn('font-black', className)} {...rest}>
      {children}
    </h1>
  )
}

export function TypoH2(props: TypoHProps) {
  const { className, children, ...rest } = props

  return (
    <h2 className={cn('font-extrabold', className)} {...rest}>
      {children}
    </h2>
  )
}
