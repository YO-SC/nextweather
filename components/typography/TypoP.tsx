import { cn } from '@/lib/utils'
import { TypoPProps } from './types'

export function TypoP(props: TypoPProps) {
  const { className, children, ...rest } = props

  return (
    <p className={cn(className)} {...rest}>
      {children}
    </p>
  )
}
