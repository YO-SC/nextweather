import { HTMLAttributes } from 'react'

interface TypoHProps extends HTMLAttributes<HTMLHeadingElement> {}
interface TypoPProps extends HTMLAttributes<HTMLHeadingElement> {}

export type { TypoHProps, TypoPProps }
