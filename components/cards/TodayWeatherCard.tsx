import { Current, Location } from '@/types/weather-api'
import Image from 'next/image'
import { IconType } from 'react-icons'
import {
  PiCloud,
  PiCloudRain,
  PiDrop,
  PiRuler,
  PiSun,
  PiThermometer,
  PiWind,
} from 'react-icons/pi'

interface TodayWeatherCardProps {
  currentData: Current
  locationData: Location
}

interface DataCardProps {
  icon: IconType
  label: string
  data: string | number
  dataTypeLabel?: string
}

function DataMiniCard(props: DataCardProps) {
  const { icon: Icon, label, data, dataTypeLabel } = props

  return (
    <div className="flex gap-2 items-center">
      <Icon />
      <div className="flex flex-col">
        <p className="text-sm">{label}</p>
        <p>
          {data} {dataTypeLabel}
        </p>
      </div>
    </div>
  )
}

export function TodayWeatherCard(props: TodayWeatherCardProps) {
  const { currentData, locationData } = props

  return (
    <div className="flex flex-col gap-2 p-4 rounded-lg md:flex-row md:justify-between bg-blue-400 text-blue-50">
      <div className="flex gap-2 items-center">
        <Image
          src={`https:${currentData.condition.icon}`}
          alt={`${currentData.condition.text} icon`}
          width={64}
          height={64}
        />

        <div>
          <p className="text-xl font-bold">{currentData.condition.text}</p>
          <p className="text-sm">
            {locationData.name}, {locationData.region}
          </p>
          <p className="text-sm">{locationData.localtime}</p>
        </div>
      </div>

      <div className="grid grid-cols-2 gap-2 md:gap-4 md:grid-cols-4 lg:grid-cols-9">
        <DataMiniCard
          icon={() => <PiThermometer />}
          label="Temperature"
          data={currentData.temp_f}
          dataTypeLabel="&deg;F"
        />

        <DataMiniCard
          icon={() => <PiThermometer />}
          label="Feels like"
          data={currentData.feelslike_f}
          dataTypeLabel="&deg;F"
        />

        <DataMiniCard
          icon={() => <PiDrop />}
          label="Humidity"
          data={currentData.humidity}
          dataTypeLabel="%"
        />

        <DataMiniCard
          icon={() => <PiRuler />}
          label="Pressure"
          data={currentData.pressure_in}
          dataTypeLabel="In"
        />

        <DataMiniCard
          icon={() => <PiCloudRain />}
          label="Precipitation"
          data={currentData.precip_in}
          dataTypeLabel="In"
        />

        <DataMiniCard
          icon={() => <PiCloud />}
          label="Cloud cover"
          data={currentData.cloud}
          dataTypeLabel="%"
        />

        <DataMiniCard
          icon={() => <PiWind />}
          label="Wind"
          data={currentData.wind_mph}
          dataTypeLabel="Mi/h"
        />

        <DataMiniCard
          icon={() => <PiWind />}
          label="Gusts"
          data={currentData.gust_mph}
          dataTypeLabel="Mi/h"
        />

        <DataMiniCard
          icon={() => <PiSun />}
          label="UV Index"
          data={currentData.uv}
        />
      </div>
    </div>
  )
}
