import { Hour } from '@/types/weather-api'
import Image from 'next/image'

interface HourWeatherCardProps {
  // keyIndex: Key
  hourData: Hour
}

export function HourWeatherCard(props: HourWeatherCardProps) {
  const { hourData } = props

  return (
    <div className="flex gap-2 items-center bg-blue-400 text-blue-50 p-4 rounded-lg">
      <Image
        src={`https:${hourData.condition.icon}`}
        alt={`${hourData.condition.text} icon`}
        width={64}
        height={64}
      />

      <div>
        <p className="text-xl font-bold">{hourData.condition.text}</p>
        <p className="text-sm">{hourData.time}</p>
      </div>
    </div>
  )
}
