import Link from 'next/link'
import { TypoH1 } from './typography'
import { PiGitlabLogo } from 'react-icons/pi'

export function Navbar() {
  return (
    <nav className="flex items-center justify-between mb-4">
      <TypoH1>NextWeather</TypoH1>

      <Link href={'https://gitlab.com/YO-SC/nextweather'} target="_blank">
        <PiGitlabLogo />
      </Link>
    </nav>
  )
}
