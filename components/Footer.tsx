import Link from 'next/link'

export function Footer() {
  return (
    <footer className="mt-4">
      <p>
        Made by{' '}
        <Link
          className="underline underline-offset-2 hover:cursor-pointer hover:text-black"
          href={'https://gitlab.com/YO-SC'}
          target="_blank"
        >
          Ysael Sáez
        </Link>
        . Powered by{' '}
        <Link
          className="underline underline-offset-2 hover:cursor-pointer hover:text-black"
          href={'https://www.weatherapi.com/'}
          title="Weather API"
          target="_blank"
        >
          WeatherAPI.com
        </Link>
        .
      </p>
    </footer>
  )
}
