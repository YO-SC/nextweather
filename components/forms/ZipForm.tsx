import { searchByZipCode } from '@/app/actions'
import { PiMagnifyingGlass } from 'react-icons/pi'

export function ZipForm() {
  return (
    <form className="flex flex-row gap-2" action={searchByZipCode}>
      <input
        className="px-4 py-2 bg-blue-100 rounded-full text-blue-950"
        type="text"
        name="zipCode"
        placeholder="Zip Code"
        pattern="^[0-9]*$"
        required
      />
      <button
        className="bg-blue-400 text-blue-50 p-4 rounded-full hover:cursor-pointer"
        type="submit"
      >
        <PiMagnifyingGlass />
      </button>
    </form>
  )
}
